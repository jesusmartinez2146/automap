// Variables
var marker,
    map,
    infowindow;

// Map initialization
function initMap () {
    map = new google.maps.Map(document.querySelector("#map"), {
        zoom: 4,
        center:{ lat: -33, lng: -70 }
      });
}

// Main Event Dispatch
$('#btn').click(() => {
    $.ajax({
        type: "post",
        url: "process.php",
        beforeSend: function(){
            $('#btn').html("Loading...");
            $('#btn').prop("disabled",true);
            $('#btn').addClass("btn-secondary");
            $('#random').html(``);
            if(marker) marker = null;
            if (infowindow) {
                infowindow.close();
            }
        },
        success: function (response) {
            $('#btn').prop("disabled",false);
            $('#btn').html("Ejecutar Proceso");
            $('#btn').removeClass("btn-secondary");
            
            r = JSON.parse(atob(response));

            $('#random').html(`Segundo Seleccionado: ${r.random}`);

            // Marker
            const LatLng = { lat: Number(r.latitude), lng: Number(r.longitude) };
            
            marker = new google.maps.Marker({
                position: LatLng,
                map,
              });
            
            //   Map center
            window.map.panTo(LatLng);
            map.setZoom(15);

            const contentString = `<img  src="${r.image}" width="250px" />`;
            infowindow = new google.maps.InfoWindow({
                content: contentString,
            });

            // Add infowindow
            marker.addListener("click", () => {
                infowindow.open({
                  anchor: marker,
                  map,
                  shouldFocus: false,
                });
              });

        }
    });
})