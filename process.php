<?php 
/**
 * @author: Jesus Martinez - Software Engineer 
 * @since: 03-07-2021
 * 
 * Test project for AutoMap
 * 
 */

 //  Load Composer 
require 'vendor/autoload.php';


// // Exiftools
use Monolog\Logger;
use PHPExiftool\Reader;

// FFMPEG
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Coordinate\TimeCode;

//  Exiftools Instance
$logger = new Logger('exiftool');
$reader = Reader::create($logger);

// FFMpeg Instance
$ffmpeg = FFMpeg::create();


// Setting Variables
$videoFile = './src/prueba.mp4';
$frameRoute = './src/images/'.time().'.jpg';

// Reading the video metadata with exiftools
$metadata = $reader->files($videoFile)->first();
$duration = $metadata->getMetadatas()->get('QuickTime:Duration')->getValue()->asString();
$latitude = $metadata->getMetadatas()->get('Composite:GPSLatitude')->getValue()->asString();
$longitude = $metadata->getMetadatas()->get('Composite:GPSLongitude')->getValue()->asString();

// Select random second
$random = rand(0,$duration);


// Loading Video
$video = $ffmpeg->open($videoFile);
$video
    ->frame(TimeCode::fromSeconds($random))
    ->save($frameRoute);


// Returning Data
echo base64_encode(json_encode(array(
    "random" => $random,
    "latitude" => $latitude,
    "longitude" => $longitude,
    "image" => $frameRoute
)));







?>
