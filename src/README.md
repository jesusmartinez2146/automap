Este sistema desarrollado con PHP 8.0 utiliza EXIFTOOLS, FFMPEG y Google Maps API para procesar un video, extraer un fotograma de un segundo aleatorio y a través de su metadata, crear un pincho en el mapa que permite ver el fotograma creado.

Requisitos Previos:

PHP >= 7.2.0
Apache 2
Composer
FFMpeg


Pasos para Clonar este repositorio

Abre una terminar y ejecuta el siguiente comando:

git clone https://jesusmartinez2146@bitbucket.org/jesusmartinez2146/automap.git

cd automap && composer update

Y ya estaría listo para ejecutarse, en el repositorio se incluye el video tutorial.mp4 donde se explica mas a fondo el desarrollo


